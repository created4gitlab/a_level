// Написати функцію, що буде сортувати числа, наприклад від 1 до 20 та вивести сортовані числа на екран

let arr = []
for (let i = 20; i > 0; i--) {
  arr.push(i)
}
function sortFunc(a) {
  let b, leng = arr.length
  for (let i = 0; i < leng; i++) {
    for (let y = i + 1; y < leng; y++) {
      if (arr[i] > arr[y]) {
        b = arr[i]
        arr[i] = arr[y]
        arr[y] = b
      }
    }
    return arr
  }
}
console.log(sortFunc(arr))


let arr = []
for (let i = 20; i > 0; i--) {
  arr.push(i)
}
let sortFunc = function (a) {
  let b, leng = arr.length
  for (let i = 0; i < leng; i++) {
    for (let y = i + 1; y < leng; y++) {
      if (arr[i] > arr[y]) {
        b = arr[i]
        arr[i] = arr[y]
        arr[y] = b
      }
    }
    return arr
  }
}
console.log(sortFunc(arr))


let arr = []
for (let i = 20; i > 0; i--) {
  arr.push(i)
}
let sortFunc = (a) => {
  let b, leng - arr.length
  for (let i = 0; i < leng; i++) {
    for (let y = i + 1; y < leng; y++) {
      if (arr[i] > arr[y]) {
        b = arr[i]
        arr[i] = arr[y]
        arr[y] = b
      }
    }
    return arr
  }
}
console.log(sortFunc(arr))


// Є масив об'єктів salaries [{name:'ivan', salary:300}, {name:'vova', salary:500}, {name:'zigmund', salary:1300}], що містять заробітні плати. 
// Напишіть функцію sumSalaries(salaries), яка повертає суму всіх зарплат - використовуючи вбудовані функції масивів. 
// Якщо об'єкт salaries порожній, то результат повинен бути 0. Виведіть на екран результат.

let salaries = [{ name: 'ivan', salary: 300 }, { name: 'vova', salary: 500 }, { name: 'zigmund', salary: 1300 }]
function sumSalaries(salaries) {
  if (salaries) {
    let sum = salaries.values(), sum1 = 0
    for (let i of sum) {
      sum1 += i.salary
    }
    return sum1
  }
  else {
    return sum1
  }
}
console.log(sumSalaries(salaries))


let salaries = [{ name: 'ivan', salary: 300 }, { name: 'vova', salary: 500 }, { name: 'zigmund', salary: 1300 }]
let sumSalaries = function (salaries) {
  if (salaries) {
    let sum = salaries.values(), sum1 = 0
    for (let i of sum) {
      sum1 += i.salary
    }
    return sum1
  }
  else {
    return sum1
  }
}
console.log(sumSalaries(salaries))

let salaries = [{ name: 'ivan', salary: 300 }, { name: 'vova', salary: 500 }, { name: 'zigmund', salary: 1300 }]
let sumSalaries = (salaries) => {
  if (salaries) {
    let sum = salaries.values(), sum1 = 0
    for (let i of sum) {
      sum1 += i.salary
    }
    return sum1
  }
  else {
    return sum1
  }
}
console.log(sumSalaries(salaries))




// Створити функцію, що в залежності від зарплати групує співробітників 300-600 середній оклад, // 1000-2000 - високий:отримати об'єкт {high:[people with salary], middle:[people with salary], //low:[ppl with salary].
// Якщо категорія пуста, має бути пустий масив. 

let salaries = [{ name: 'ivan', salary: 300 }, { name: 'vova', salary: 500 }, { name: 'zigmund', salary: 1300 }]
function sortSalary(salaries) {
  let people = { low: [], middle: [], high: [] }
  if (salaries) {
    let salary = salaries.values()
    for (let i of salary) {
      if (i.salary < 300 && i.salary != 0) {
        people.low.push(i.name)
      }
      else if (i.salary >= 300 && i.salary < 600) {
        people.middle.push(i.name)
      }
      else if (i.salary > 1000 && i.salary < 2000) {
        people.high.push(i.name)
      }
      else if (i.salary == null) {
        return people
      }
    }
    return people
  }
}
console.log(sortSalary(salaries))


let salaries = [{ name: 'ivan', salary: 300 }, { name: 'vova', salary: 500 }, { name: 'zigmund', salary: 1300 }]
let sortSalary = function (salaries) {
  let people = { low: [], middle: [], high: [] }
  if (salaries) {
    let salary = salaries.values()
    for (let i of salary) {
      if (i.salary < 300 && i.salary != 0) {
        people.low.push(i.name)
      }
      else if (i.salary >= 300 && i.salary < 600) {
        people.middle.push(i.name)
      }
      else if (i.salary > 1000 && i.salary < 2000) {
        people.high.push(i.name)
      }
      else if (i.salary == null) {
        return people
      }
    }
    return people
  }
}
console.log(sortSalary(salaries))


let salaries = [{ name: 'ivan', salary: 300 }, { name: 'vova', salary: 500 }, { name: 'zigmund', salary: 1300 }]
let sortSalary = (salaries) => {
  let people = { low: [], middle: [], high: [] }
  if (salaries) {
    let salary = salaries.values()
    for (let i of salary) {
      if (i.salary < 300 && i.salary != 0) {
        people.low.push(i.name)
      }
      else if (i.salary >= 300 && i.salary < 600) {
        people.middle.push(i.name)
      }
      else if (i.salary > 1000 && i.salary < 2000) {
        people.high.push(i.name)
      }
      else if (i.salary == null) {
        return people
      }
    }
    return people
  }
}
console.log(sortSalary(salaries))

