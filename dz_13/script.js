// 1. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// Развернуть этот массив в обратном направлении
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let newArr = []
let i = 1
let leng = arr.length
while (i <= leng) {
    newArr.push(arr[leng - i])
    i++
}
console.log(newArr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let newArr = []
let leng = arr.length
for (let i = 1; i <= leng; i++) {
    newArr.push(arr[leng - i])
}
arr = newArr
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let newArr = []
let leng = arr.length
for (let i in arr) {
  newArr.push(arr[(leng - 1) - i])
}
arr = newArr
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let newArr = []
let leng = arr.length
for (let i of arr) {
    newArr.push(arr[(leng - 1) - arr.indexOf(i)])
}
arr = newArr
console.log(arr)


// 2. Дан массив
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// Развернуть этот массив в обратном направлении
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69], arr2 = []
let i = 1
let leng = arr.length
while (i <= leng) {
  arr2.push(arr[leng - i])
  i++
}
arr = arr2
console.log(arr)

let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let i = 2
let leng = arr.length
for (let i = 2; i <= leng; i++) {
    arr.push(arr[leng - i])
    arr.splice(leng - (i + 1), 1)
}
console.log(arr)

let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69], arr2 = []
let leng = arr.length
for (let i in arr) {
  arr2.push(arr[(arr.length - 1) - i])
}
arr = arr2
console.log(arr)

let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69], arr2 = []
let leng = arr.length
for (let i of arr) {
  arr2.unshift(i)
}
arr = arr2
console.log(arr)

// 3. Дана строка
// let str = 'Hi I am ALex'
// развенуть строку в обратном направлении.
let str = 'Hi I am ALex'
let str2 = ""
let i = 1
let leng = str.length
while (i <= leng) {
    str2 += str[leng - i]
    i++
}
console.log(str2)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i = 1; i <= leng; i++) {
    str2 += str[leng - i]
}
console.log(str2)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i in str) {
    str2 += str[(leng - 1) - i]
}
console.log(str2)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
let z = 1
for (let i of str) {
    str2 += str[leng - z]
    z++
}
console.log(str2)

// 4. Дана строка 
// let str = 'Hi I am ALex'
// сделать ее с с маленьких букв
let str = 'Hi I am ALex'
let i = 0
let str2 = ""
let leng = str.length
while (i < leng) {
  if (str.charCodeAt(i) >= 65 && str.charCodeAt(i) <= 90) {
    str2 += String.fromCharCode(str.charCodeAt(i) + 32)
  }
  else {
    str2 += str[i]
  }
  i++
}
str = str2
console.log(str)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i = 0; i < leng; i++) {
  if (str.charCodeAt(i) >= 65 && str.charCodeAt(i) <= 90) {
    str2 += String.fromCharCode(str.charCodeAt(i) + 32)
  }
  else {
    str2 += str[i]
  }
}
str = str2
console.log(str)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i in str) {
  str2 += str[i].toLocaleLowerCase()
}
str = str2
console.log(str)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i of str) {
  str2 += i.toLocaleLowerCase()
}
str = str2
console.log(str)

// 5. Дана строка 
// let str = 'Hi I am ALex'
// сделать все буквы большие
let str = 'Hi I am ALex'
let i = 0
let str2 = ""
let leng = str.length
while (i < leng) {
  if (str.charCodeAt(i) >= 97 && str.charCodeAt(i) <= 122) {
    str2 += String.fromCharCode(str.charCodeAt(i) - 32)
  }
  else {
    str2 += str[i]
  }
  i++
}
str = str2
console.log(str)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i = 0; i < leng; i++) {
  if (str.charCodeAt(i) >= 97 && str.charCodeAt(i) <= 122) {
    str2 += String.fromCharCode(str.charCodeAt(i) - 32)
  }
  else {
    str2 += str[i]
  }
}
str = str2
console.log(str)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i in str) {
  str2 += str[i].toLocaleUpperCase()
}
str = str2
console.log(str)

let str = 'Hi I am ALex'
let str2 = ""
let leng = str.length
for (let i of str) {
  str2 += i.toLocaleUpperCase()
}
str = str2
console.log(str)

// 7. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с маленькой
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let i = 0
let leng = arr.length
while (i < leng) {
    arr[i] = arr[i].toLocaleLowerCase()
    i++
}
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let leng = arr.length
for (let i = 0; i < leng; i++) {
    arr[i] = arr[i].toLocaleLowerCase()
}
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
for (let i in arr) {
    arr[i] = arr[i].toLocaleLowerCase()
}
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let y = 0
for (let i of arr) {
  arr[y] = i.toLocaleLowerCase()
  y++
}
console.log(arr)

// 8. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с большой
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let leng = arr.length
let i = 0
while (i < leng) {
    arr[i] = arr[i].toLocaleUpperCase()
    i++
}
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let leng = arr.length
for (let i = 0; i < leng; i++) {
    arr[i] = arr[i].toLocaleUpperCase()
}
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
for (let i in arr) {
    arr[i] = arr[i].toLocaleUpperCase()
}
console.log(arr)

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let y = 0
for (let i of arr) {
  arr[y] = i.toLocaleUpperCase()
  y++
}
console.log(arr)


// 9. Дано число
// let num = 1234678
// развернуть ее в обратном направлении
let num = 1234678
let str = String(num)
let str2 = ""
let i = 1
let leng = str.length
while (i <= leng) {
  str2 += str[leng - i]
  i++
}
num = Number(str2)
console.log(num)

let num = 1234678
let str = String(num)
let str2 = ""
let leng = str.length
for (let i = 1; i <= leng; i++) {
  str2 += str[leng - i]
}
num = Number(str2)
console.log(num)

let num = 1234678
let str = String(num)
let str2 = ""
let leng = str.length
for (let i in str) {
  str2 += str[(leng - 1) - i]
}
num = Number(str2)
console.log(num)

let num = 1234678
let str = String(num)
let str2 = ""
let y = 0
let leng = str.length
for (let i of str) {
  str2 += str[(leng - 1) - y]
  y++
}
num = Number(str2)
console.log(num)

// 10. Дан масси 
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// отсортеруй его в порядке убывания
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let i = 0, a
let leng = arr.length
while (i < leng) {
  let y = i + 1
  while (y < leng) {
    if (arr[i] < arr[y]) {
      a = arr[i]
      arr[i] = arr[y]
      arr[y] = a
    }
    y++
  }
  i++
}
console.log(arr)

let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let a
let leng = arr.length
for (let i = 0; i < leng; i++) {
  for (let y = i + 1; y < leng; y++) {
    if (arr[i] < arr[y]) {
      a = arr[i]
      arr[i] = arr[y]
      arr[y] = a
    }
  }
}
console.log(arr)

let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let a
for (let i in arr) {
  for (let y in arr) {
    if (arr[i] > arr[y]) {
      a = arr[y]
      arr[y] = arr[i]
      arr[i] = a
    }
  }
}
console.log(arr)

// с защитой от повтора при большем к-стве
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let x = 0
for (let i of arr) {
  let z = 0
  for (let y of arr) {
    if (y < i) {
      arr[z] = i
      i = y
      arr[x] = i
    }
    z++
  }
  x++
}
console.log(arr)

