// 1. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// Реализовать все буквы маленькие, пример вызовать метода
// test(arr, val => val.toLowerCase())
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
function lowerCase(arr1, val) {
  let i = 0, leng = arr1.length
  while (i < leng) {
    arr[i] = val(arr[i])
    i++
  }
  return arr
}
console.log(lowerCase(arr, val => val.toLowerCase()))

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let lowerCase = function (arr1, val) {
  let i = 0, leng = arr1.length
  do {
    arr[i] = val(arr[i])
    i++
  } while (i < leng)
  return arr
}
console.log(lowerCase(arr, val => val.toLowerCase()))


let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let lowerCase = (arr1, val) => {
  let leng = arr1.length
  for (let i = 0; i < leng; i++) {
    arr[i] = val(arr[i])
  }
  return arr
}
console.log(lowerCase(arr, val => val.toLowerCase()))


let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let lowerCase = (arr1, val) => {
  for (let i in arr) {
    arr[i] = val(arr[i])
  }
  return arr
}
console.log(lowerCase(arr, val => val.toLowerCase()))

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let lowerCase = (arr1, val) => {
  let j = 0
  for (let i of arr) {
    arr[j] = val(i)
    j++
  }
  return arr
}
console.log(lowerCase(arr, val => val.toLowerCase()))


// 2. Дан массив
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// Упорядочить массив
// test(arr, val => правила сравнения asc или desc)

let mass = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function sortMain(arr, val) {
  let j = 0, leng = arr.length
  do {
    let b, z = 0
    do {
      if (val(arr[j], arr[z])) {
        b = arr[z]
        arr[z] = arr[j]
        arr[j] = b
      }
      z++
    } while (z < leng)
    j++
  } while (j < leng)
  return arr
}
console.log(sortMain(mass, (a, b) => a < b))


let mass = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function sortMain(arr, val) {
  let leng = arr.length
  for (let j = 0; j < leng; j++) {
    let b
    for (let z = 0; z < leng; z++) {
      if (val(arr[j], arr[z])) {
        b = arr[z]
        arr[z] = arr[j]
        arr[j] = b
      }
    }
  }
  return arr
}
console.log(sortMain(mass, (a, b) => a > b))


let mass = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function sortMain(arr, val) {
  for (let j in arr) {
    let b
    for (let z in arr) {
      if (val(arr[j], arr[z])) {
        b = arr[z]
        arr[z] = arr[j]
        arr[j] = b
      }
    }
  }
  return arr
}
console.log(sortMain(mass, (a, b) => a > b))



let mass = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function sortMain(arr, val) {
  let j = 0, leng = arr.length
  while (j < leng) {
    let b, z = 0
    while (z < leng) {
      if (val(arr[j], arr[z])) {
        b = arr[z]
        arr[z] = arr[j]
        arr[j] = b
      }
      z++
    }
    j++
  }
  return arr
}
console.log(sortMain(mass, (a, b) => a > b))


let mass = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function sortMain(arr, val) {
  let x = 0
  for (let j of arr) {
    let b, y = 0
    for (let z of arr) {
      if (val(j, z)) {
        b = z
        arr[y] = j
        arr[x] = b
        j = z
      }
      y++
    }
    x++
  }
  return arr
}
console.log(sortMain(mass, (a, b) => a > b))



// 3. Задача с классами. Реализовать класс User, User будет иметь конструктор (firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty). 
// Реализовать два класса наследника:
// - Студент (добавить в конструктор grade => средний бал)
// - Подготовительная группа (issueOrDeduction => true or false)
// Затем реализовать методы
// - метод сокращение с поного имени на короткое (Забара Александр Сергеевич => Забара А.С.)
// - метод сделает заглавными только первые буквы
// - метод возвращает длительность учебы в годах
// - метод возвращает длительность учебы в месяцах
// - метод возвращает возраст студента
// - метод возвращает диплом или диплом не получен (100 балом макс, нет диплома меньше 60 балов)

class User {
  constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty) {
    this.firstName = firstName
    this.secondName = secondName
    this.lastName = lastName
    this.age = age
    this.receiptDate = receiptDate
    this.dateOfIssue = dateOfIssue
    this.specialty = specialty
  }
  short() {
    return this.secondName + " " + this.firstName[0] + '. ' + this.lastName[0] + '.'
  }
  upperCase() {
    function toUpCase(name) {
      let i = 0, upperName = " "
      while (i < name.length) {
        if (i === 0) {
          upperName += name[i].toUpperCase()
        }
        else { upperName += name[i] }
        i++
      }
      return upperName
    }
    return toUpCase(this.secondName) + " " + toUpCase(this.firstName) + " " + toUpCase(this.lastName)
  }
  getAge() {
    return "возраст " + this.age
  }
  yearLong() {
    function dateY(x, y) {
      let monthDate = (num) => {
        let i = 0, month = ""
        month += num[3] + num[4]
        return Number(month)
      }
      let yearDate = (num) => {
        let year = ""
        year += num[6] + num[7]
        return Number(year)
      }
      if (monthDate(y) === monthDate(x)) {
        return "Проучился " + (yearDate(y) - yearDate(x)) + " лет/год(а)"
      }
      else if (monthDate(y) > monthDate(x)) {
        return "Проучился " + (yearDate(y) - yearDate(x)) + " лет/год(а) " + (monthDate(y) - monthDate(x)) + " месяца(ов)"
      }
      else return "Проучился " + (yearDate(y) - yearDate(x) - 1) + " лет/год(а) " + (12 - (monthDate(x) - monthDate(y))) + " месяца(ов)"
    }
    return dateY(this.receiptDate, this.dateOfIssue)
  }
  monthLong() {
    function dateM(x, y) {
      let monthDate = (num) => {
        let i = 0, month = ""
        month += num[3] + num[4]
        return Number(month)
      }
      let yearDate = (num) => {
        let year = ""
        year += num[6] + num[7]
        return Number(year)
      }
      if (monthDate(y) === monthDate(x)) {
        return "Проучился " + ((yearDate(y) - yearDate(x)) * 12) + " месяцов"
      }
      else if (monthDate(y) > monthDate(x)) {
        return "Проучился " + Number(((yearDate(y) - yearDate(x)) * 12) + (monthDate(y) - monthDate(x))) + " месяца(ов)"
      }
      else
        return "Проучился " + Number(((yearDate(y) - yearDate(x) - 1) * 12) + (12 - (monthDate(x) - monthDate(y)))) + " месяца(ов)"
    }
    return dateM(this.receiptDate, this.dateOfIssue)
  }
}
class Student extends User {
  constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty, grade) {
    super(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty)
    this.grade = grade
  }
  ifDiplom() {
    return this.grade >= 60 && this.grade <= 100 ? "Есть диплом" : "Нет диплома"
  }
}
class PrepGroup extends User {
  constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty, issueOrDeduction) {
    super(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty)
    this.issueOrDeduction = "true or false"
  }
}
let student1 = new Student("Vlad", "Koretskiy", "Alexandrovich", 23, "10.12.16", "08.10.21", "IT", 60)
console.log(student1.monthLong())


