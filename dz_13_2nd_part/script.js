// 1. Данн массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Определить минимальное число
// -Определить максимальное число
// -Определить среднее число между этих чисел

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let i = 0
let maxNum = arr[0], minNum = arr[0], aveNum = arr[0]
let leng = arr.length
while (i < leng) {
  if (arr[i] > maxNum) {
    maxNum = arr[i]
  }
  else if (arr[i] < minNum) {
    minNum = arr[i]
  }
  aveNum = (maxNum+minNum)/2  // можно и в условие загнать
  i++
}

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let maxNum = arr[0], minNum = arr[0], aveNum = arr[0]
let leng = arr.length
for (let i = 0; i < leng; i++) {
  if (arr[i + 1] > maxNum) {
    maxNum = arr[i]
  }
  else if (arr[i + 1] < minNum) {
    minNum = arr[i + 1]
  }
  aveNum = (maxNum+minNum)/2  
}


let arr = [12, 22, -1245, -1, 0, 4994, 555]
let maxNum = arr[0], minNum = arr[0], aveNum = arr[0]
let leng = arr.length
for (let i in arr) {
  if (arr[i] > maxNum) {
    maxNum = arr[i]
  }
  else if (arr[i] < minNum) {
    minNum = arr[i]
  }
aveNum = (maxNum+minNum)/2
}

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let maxNum = arr[0], minNum = arr[0], aveNum = arr[0]
let leng = arr.length
for (let i of arr) {
  if (i > maxNum) {
    maxNum = i
  }
  else if (i < minNum) {
    minNum = i
  }
aveNum = (maxNum+minNum)/2
}


// 2. Данн массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Посчитать длинну массива, нельзя использовать .length

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let arrLength = 0, i = 0, leng = arr.length
while (i < leng) {
  arrLength++
  i++
}
console.log(arrLength)


let arr = [12, 22, -1245, -1, 0, 4994, 555]
let arrLength = 0, leng = arr.length
for (let i = 0; i < leng; i++) {
  arrLength++
}
console.log(arrLength)

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let arrLength = 0
for (let i in arr) {
  arrLength++
}
console.log(arrLength)

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let arrLength = 0
for (let i of arr) {
  arrLength++
}
console.log(arrLength)


// 3. Данн массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Сделать положительные числа отрицательными
// -Сделать отрицательные числа положительными

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let i = 0, leng = arr.length
while (i < leng) {
  if (arr[i] > 0) {
    arr[i] = -arr[i]
  }
  else if (arr[i] < 0) {
    arr[i] = -arr[i]
  }
  i++
}
console.log(arr)


let arr = [12, 22, -1245, -1, 0, 4994, 555]
let leng = arr.length
for (let i = 0; i < leng; i++) {
  if (arr[i] > 0) {
    arr[i] = -arr[i]
  }
  else if (arr[i] < 0) {
    arr[i] = -arr[i]
  }
}
console.log(arr)

let arr = [12, 22, -1245, -1, 0, 4994, 555]
for (let i in arr) {
  if (arr[i] > 0) {
    arr[i] = -arr[i]
  }
  else if (arr[i] < 0) {
    arr[i] = -arr[i]
  }
}
console.log(arr)


let arr = [12, 22, -1245, -1, 0, 4994, 555]
let z = 0
for (let i of arr) {
  if (i > 0) {
    arr[z] = -i
  }
  else if (i < 0) {
    arr[z] = -i
  }
  z++
}
console.log(arr)


// 3. Данн массив чисел [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
// -удалить только отрицательные числа
// -удалить все числа которые больше 12

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555], arr2 = []
let i = 0,
  leng = arr.length;
while (i < leng) {
  if (arr[i] >= 0 && arr[i] <= 12) {
arr2.push(arr[i])
  }
  i++;
}
arr = arr2
console.log(arr);

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555],
  arr2 = [];
let leng = arr.length;
for (let i = 0; i < leng; i++) {
  if (arr[i] >= 0 && arr[i] <= 12) {
    arr2.push(arr[i]);
  }
}
arr = arr2;
console.log(arr);

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
for (let i in arr) {
  for (let y in arr) {
    if (arr[y] < 0 || arr[y] > 12) {
      arr.splice(y, 1)
    }
  }
}
console.log(arr)


let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
let z = 0
for (let i of arr) {
  for (let y of arr) {
    if (y < 0 || y > 12) {
      arr.splice(arr.indexOf(y), 1)
    }
    z++
  }
}
console.log(arr)


// 4. Дан массив массивов 
// Отсортировать массивы по годам,

let arr = [
  [
    car => "Nissan",
    model => "Leaf",
    color => "green",
    year => 2015,
    engine => 1.6
  ],
  [
    car => "bmw",
    model => "i3",
    color => "black",
    year => 2018,
    engine => 2.0
  ],
  [
    car => "bmw",
    model => "x5",
    color => "orange",
    year => 2016,
    engine => 3.0
  ],
  [
    car => "mersedes",
    model => "e220",
    color => "blue",
    year => 2014,
    engine => 2.0
  ],
  [
    car => "волга",
    model => "2410",
    color => "black",
    year => 1989,
    engine => 2.4
  ],
]
let i = 0, a, leng = arr.length
while (i < leng) {
  let y = 0
  while (y < leng) {
    if (String(arr[i][3]) < String(arr[y][3])) {
      a = arr[i]
      arr[i] = arr[y]
      arr[y] = a
    }
    y++
  }
  i++
}


let a, leng = arr.length
for (let i = 0; i < leng; i++) {
  for (let y = 0; y < leng; y++) {
    if (String(arr[i][3]) < String(arr[y][3])) {
      a = arr[i]
      arr[i] = arr[y]
      arr[y] = a
    }
  }
}


let a
for (let i in arr) {
  for (let y in arr) {
    if (String(arr[i][3]) < String(arr[y][3])) {
      a = arr[i]
      arr[i] = arr[y]
      arr[y] = a
    }
  }
}

let a,
  z = 0;
for (let i of arr) {
  let x = 0;
  for (let y of arr) {
    if (String(i[3]) < String(y[3])) {
      (a = i), (arr[z] = y);
      arr[x] = a;
      i = y;
    }
    x++;
  }
  z++;
}
