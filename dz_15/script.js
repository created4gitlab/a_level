// Найти площадь
function square(a, b) {
    return a * b
  }
  console.log(square(2, 3))
  
  
  let square = function (a, b) {
    return a * b
  }
  console.log(square(2, 3))
  
  
  let square = (a, b) => {
    return a * b
  }
  console.log(square(2, 3))
  
  // Теорема Пифагора
  let pifagor = (a, b) => {
    let c = a ** 2 + b ** 2
    return c
  }
  console.log(pifagor(2, 3))
  
  
  let pifagor = function (a, b) {
    let c = a ** 2 + b ** 2
    return c
  }
  console.log(pifagor(2, 3))
  
  
  function pifagor(a, b) {
    let c = a ** 2 + b ** 2
    return c
  }
  console.log(pifagor(2, 3))
  
  
  // Найти дискриминант
  let discr = (b, a, c) => {
    let discr = b ** 2 - 4 * a * c
    return discr
  }
  console.log(discr(2, 3, 3))
  
  let discr = function (b, a, c) {
    let discr = b ** 2 - 4 * a * c
    return discr
  }
  console.log(discr (2, 3, 3))
  
  
  let discr = (b, a, c) => {
    let discr = b ** 2 - 4 * a * c
    return discr
  }
  console.log(discr (2, 3, 3))
  
  // Создать только четные числа до 100
  let arr = []
  let chet = (arr) => {
    let i = 1, leng = 100
    while (i <= leng) {
      if (i % 2 == 0) {
        arr.push(i)
      }
      i++
    }
    return arr
  }
  console.log(chet(arr))
  
  
  let arr = []
  let chet = (arr) => {
    let i = 1, leng = 100
    do {
      if (i % 2 == 0) {
        arr.push(i)
      }
      i++
    } while (i <= leng)
    return arr
  }
  console.log(chet(arr))
  
  
  let arr = [], leng = 100
  let chet = (arr) => {
    for (let i = 1; i <= leng; i++) {
      if (i % 2 == 0) {
        arr.push(i)
      }
    }
    return arr
  }
  console.log(chet(arr))
  
  
  function chet(val) {
    let arr = [], arr2 = [], leng = 100
    for (let i = 1; i <= leng; i++) {
      arr.push(i)
    }
    for (let j of arr) {
      if (j % 2 == 0) {
        arr2.push(j)
      }
    }
    return arr2
  }
  console.log(chet())
  
  
  let chet = function (val) {
    let arr = [], arr2 = [], leng = 100
    for (let i = 1; i <= leng; i++) {
      arr.push(i)
    }
    for (let j in arr) {
      if (arr[j] % 2 == 0) {
        arr2.push(arr[j])
      }
    }
    return arr2
  }
  console.log(chet())
  
  
  
  // Создать не четные числа до 100
  let arr = [], leng = 100
  let nechet = (arr) => {
    let i = 1
    while (i <= leng) {
      if (i % 2 != 0) {
        arr.push(i)
      }
      i++
    }
    return arr
  }
  console.log(nechet (arr))
  
  
  let arr = [], leng = 100
  let nechet = (arr) => {
    let i = 1
    do {
      if (i % 2 != 0) {
        arr.push(i)
      }
      i++
    } while (i <= leng)
    return arr
  }
  console.log(nechet (arr))
  
  
  let arr = [], leng = 100
  let nechet = (arr) => {
    for (let i = 1; i <= leng; i++) {
      if (i % 2 != 0) {
        arr.push(i)
      }
    }
    return arr
  }
  console.log(nechet (arr))
  
  
  function nechet (val) {
    let arr = [], arr2 = [], leng = 100
    for (let i = 1; i <= leng; i++) {
      arr.push(i)
    }
    for (let j of arr) {
      if (j % 2 != 0) {
        arr2.push(j)
      }
    }
    return arr2
  }
  console.log(nechet ())
  
  
  let nechet = function (val) {
    let arr = [], arr2 = [], leng = 100
    for (let i = 1; i <= leng; i++) {
      arr.push(i)
    }
    for (let j in arr) {
      if (arr[j] % 2 != 0) {
        arr2.push(arr[j])
      }
    }
    return arr2
  }
  console.log(nechet())
  
  // Создать функцию по нахождению числа в степени
  function power(a, b) {
    let i = 1, power = a
    while (i < b) {
      power *= a
      i++
    }
    return power
  }
  console.log(power(3, 4))
  
  
  let power = function (a, b) {
    let i = 1, power = a
    do {
      power *= a
      i++
    } while (i < b)
    return power
  }
  console.log(power(3, 4))
  
  let power = (a, b) => {
    let power = a
    for (let i = 1; i < b; i++) {
      power *= a
    }
    return power
  }
  console.log(power(3, 4))
  
  
  let power = (a, b) => {
    let power = a, arr = []
    for (let i = 1; i < b; i++) {
      arr.push(i)
    }
    for (let i in arr) {
      power *= a
    }
    return power
  }
  console.log(power(3, 4))
  
  let power = (a, b) => {
    let power = a, arr = []
    for (let i = 1; i < b; i++) {
      arr.push(i)
    }
    for (let of in arr) {
      power *= a
    }
    return power
  }
  console.log(power(3, 4))
  
  
  // написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку. 
  // По дефолту функция сортирует в порядке возростания. Но если передать всторой параметр то функция будет сортировать по убыванию.
  // sort(arr)
  // sotr(arr, 'asc')
  // sotr(arr, 'desc')
  
  
  let mass = []
  for (let i = 0; i < 10; i++) {
    mass.push(Math.floor(Math.random() * 101))
  }
  function sortMain(arr, desc) {
    let j = 0, leng = arr.length
    do {
      let b, z = 0
      do {
        if (arr[j] < arr[z] && desc === undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
        else if (arr[j] > arr[z] && desc != undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
        z++
      } while (z < leng)
      j++
    } while (j < leng)
    return arr
  }
  console.log(sortMain(mass, 2))
  
  
  function sortMain(arr, desc) {
    let j = 0, leng = arr.length
    while (j < leng) {
      let b, z = 0
      while (z < leng) {
        if (arr[j] < arr[z] && desc === undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
        else if (arr[j] > arr[z] && desc != undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
        z++
      }
      j++
    }
    return arr
  }
  console.log(sortMain(mass, 2))
  
  
  let sortMain = function (arr, desc) {
    let leng = arr.length
    for (let j = 0; j < leng; j++) {
      let b
      for (let z = 0; z < leng; z++) {
        if (arr[j] < arr[z] && desc === undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
        else if (arr[j] > arr[z] && desc != undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
      }
    }
    return arr
  }
  console.log(sortMain(mass, 2))
  
  
  let sortMain = (arr, desc) => {
    let leng = arr.length
    for (let j in arr) {
      let b
      for (let z in arr) {
        if (arr[j] < arr[z] && desc === undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
        else if (arr[j] > arr[z] && desc != undefined) {
          b = arr[z]
          arr[z] = arr[j]
          arr[j] = b
        }
      }
    }
    return arr
  }
  console.log(sortMain(mass))
  
  
  let sortMain = (arr, desc) => {
    let leng = arr.length, x = 0
    for (let j of arr) {
      let b, y = 0
      for (let z of arr) {
        if (j < z && desc === undefined) {
          b = z
          arr[y] = j
          arr[x] = b
          j = z
        }
        else if (j > z && desc != undefined) {
          b = z
          arr[y] = j
          arr[x] = b
          j = z
        }
        y++
      }
      x++
    }
    return arr
  }
  console.log(sortMain(mass, 2))
  
  
  
  // написать функцию поиска в массиве. функция будет принимать два параметра. Первый массив, второй посковое число. search(arr, find)
  
  let mass = []
  for (let i = 0; i < 100; i++) {
    mass.push(Math.floor(Math.random() * 101))
  }
  function search(arr, find) {
    let i = 0, leng = arr.length, result
    while (i < leng) {
      if (arr[i] === find) {
        result = "Искомое число: " + arr[i] + ", " + "индекс: " + i
        break
      }
      i++
    }
    return result
  }
  console.log(search(mass, 5))
  
  
  let search = function (arr, find) {
    let i = 0, leng = arr.length, result
    do {
      if (arr[i] === find) {
        result = "Искомое число: " + arr[i] + ", " + "индекс: " + i
        break
      }
      i++
    } while (i < leng)
    return result
  }
  console.log(search(mass, 5))
  let search = (arr, find) => {
    let leng = arr.length, result
    for (let i = 0; i < leng; i++) {
      if (arr[i] === find) {
        result = "Искомое число: " + arr[i] + ", " + "индекс: " + i
        break
      }
    }
    return result
  }
  console.log(search(mass, 5))
  
  
  let search = (arr, find) => {
    let result
    for (let i in arr) {
      if (arr[i] === find) {
        result = "Искомое число: " + arr[i] + ", " + "индекс: " + i
        break
      }
    }
    return result
  }
  console.log(search(mass, 5))
  
  
  let search = (arr, find) => {
    let result
    for (let i of arr) {
      if (i === find) {
        result = "Искомое число: " + i + ", " + "индекс: " + arr.indexOf(i)
        break
      }
    }
    return result
  }
  console.log(search(mass, 5))  
